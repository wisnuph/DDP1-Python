density = float(input("Masukkan nilai densitas batuan: "))

if 1.4 <= density <= 1.7:
    print("\nTermasuk ke dalam kelompok batuan Unconsolidated\nJenis batuan berupa Pyroclastic\n")
elif 2.0 <= density < 2.7:
    print("Termasuk ke dalam kelompok batuan Sedimentary")
    if 2.0 <= density < 2.5:
        print("Jenis batuan berupa Salt")
    else:
        print("Jenis batuan berupa Limestone")
elif 2.7 <= density < 3.0:
    print("Termasuk ke dalam kelompok batuan Igneous")
    if 2.7 <= density < 2.8:
        print("Jenis batuan berupa Granite")
    else:
        print("Jenis batuan berupa Basalt")
elif 3.0 <= density <= 7.6:
    print("Termasuk ke dalam kelompok batuan Metamorphic")
    if 3.0 <= density <= 3.1:
        print("Jenis batuan berupa Gneiss")
    elif 4.9 <= density <= 5.2:
        print("Jenis batuan berupa Pyrite")
    else:
        print("Jenis batuan berupa Galena")
else:
    print("\nTidak tergolong ke dalam kelompok batuan\nMungkin bukan sebuah batu\n")