import modul_atm

def dict_hasil_registrasi():
    try:
        data = open('nasabah_awal.txt', 'r') ; data_regis = {}
        for line in data:
            nama, kota, kategori = line.split()
            if kota == 'JAKARTA' or kota == 'BEKASI':
                if nama.find('ï»¿') != -1:
                    nama = nama.split('ï»¿')
                    key = modul_atm.registrasi(nama[1],kota,kategori)
                    data_regis[key] = []
                else:
                    key = modul_atm.registrasi(nama,kota,kategori)
                    data_regis[key] = []
            else:
                key = modul_atm.registrasi(nama,'LAINNYA',kategori)
                data_regis[key] = []
        return data_regis
        data.close()
    except FileNotFoundError:
        return None

data = dict_hasil_registrasi()
if data == None:
    print('File tidak ditemukan')
else:
    pilihan = ''
    while pilihan != '3':
        pilihan = input('Pilih menu (1.Transfer 2.Cek riwayat 3.Keluar): ')
        if pilihan == '1':
            tf_data = input('Masukkan perintah transfer (format: transfer [nama nasabah pengirim] [nama nasabah penerima] [nominal transfer]): ')
            try:
                tf_data = tf_data.split(' ')
                if tf_data[1] == tf_data[2]:
                    print('Nama yang diinput harus berbeda')
                else:
                    if int(tf_data[3]) > 9000000:
                        print('Maksimal transfer yang diinput sebesar 9000000')
                    elif int(tf_data[3]) <= 0:
                        print('Nominal transfer harus lebih besar dari 0')
                    else:
                        transaksi = modul_atm.data_transfer(tf_data[1].upper(), tf_data[2].upper(), int(tf_data[3]), data)
                        if transaksi == False:
                            print('Nasabah pengirim/penerima tidak terdaftar pada sistem ATM')
                        else:
                            print('Transfer berhasil')
            except IndexError:
                print('Masukkan perintah sesuai format')
            except ValueError:
                print('Masukkan nominal transfer dengan angka')
        elif pilihan == '2':
            nasabah = input('Masukkan nama nasabah: ')
            if modul_atm.cek_nama(nasabah.upper(), data) == None:
                print('Nama nasabah tidak terdaftar pada sistem ATM')
            else:
                if data[modul_atm.cek_nama(nasabah.upper(), data)] == []:
                    print('Belum melakukan transaksi apapun')
                else:
                    print('Riwayat', nasabah.capitalize()+':')
                    for value in data[modul_atm.cek_nama(nasabah.upper(), data)]:
                        for isi in data.keys():
                            if isi.find(value[:9]) != -1:
                                print('Transfer ke', isi[:isi.find(':')].capitalize(), 'sebesar', int(value[9:]))
        else:
            if pilihan == '3':
                print()
            else:
                print('Input yang dimasukkan harus sesuai menu')
    modul_atm.save_transaction(data)
    print('Keluar program. Semua transaksi disimpan di log.')



