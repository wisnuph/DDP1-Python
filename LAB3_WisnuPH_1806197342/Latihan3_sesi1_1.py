text = input("Tuliskan sebuah kalimat: ")

index = 0
vokal = 0
panjang = len(text)

while index < panjang:
    if (text[index] == "a" or text[index] == "b" or text[index] == "c" or text[index] == "d" or text[index] == "e"):
        vokal += 1
   
    index += 1

print("Jumlah huruf vokal adalah sebesar", vokal)