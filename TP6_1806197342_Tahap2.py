import urllib.request
import random

def hp_student(difficulty):
    page = urllib.request.urlopen('https://baguspribadi99.github.io/difficulty/difficulty.html')
    text = page.read().decode('utf8')
    index_sebelum = text.find(difficulty)
    index_hp_student = text.find('hp-student: ', index_sebelum)
    index_student = int(text[index_hp_student + len('hp-student: '):text.find('</li>', index_hp_student)])
    return(index_student)

def hp_kuliah(difficulty):
    page = urllib.request.urlopen('https://baguspribadi99.github.io/difficulty/difficulty.html')
    text = page.read().decode('utf8')
    index_sebelum = text.find(difficulty)
    index_hp_kuliah = text.find('hp-kuliah: ', index_sebelum)
    index_kuliah = int(text[index_hp_kuliah + len('hp-kuliah: '):text.find('</li>', index_hp_kuliah)])
    return(index_kuliah)

def attack_student(difficulty):
    page = urllib.request.urlopen('https://baguspribadi99.github.io/difficulty/difficulty.html')
    text = page.read().decode('utf8')
    index_sebelum = text.find(difficulty)
    index_attack_student = text.find('attack-student: ', index_sebelum)
    index_nilai_attack_student = int(text[index_attack_student + len('attack-student: '):text.find('</li>', index_attack_student)])
    return(index_nilai_attack_student)

def attack_kuliah(difficulty):
    page = urllib.request.urlopen('https://baguspribadi99.github.io/difficulty/difficulty.html')
    text = page.read().decode('utf8')
    index_sebelum = text.find(difficulty)
    index_attack_kuliah = text.find('attack-kuliah: ', index_sebelum)
    index_nilai_attack_kuliah = int(text[index_attack_kuliah + len('attack-kuliah: '):text.find('</li>', index_attack_kuliah)])
    return(index_nilai_attack_kuliah)

difficulty = input('Tentukan tingkat kesulitan yang Anda inginkan!  (easy, medium, hard)').lower()
hp_student = hp_student(difficulty) ; hp_kuliah = hp_kuliah(difficulty)
attack_student = attack_student(difficulty) ; attack_kuliah = attack_kuliah(difficulty)
print(f'\nBerikut atribut objek:\nStudent (Hp = {hp_student}; Attack = {attack_student})\nKuliah (Hp = {hp_kuliah}; Attack = {attack_kuliah})')
while hp_student > 0 and hp_kuliah > 0:
    action_student = input('Tentukan action untuk student:  (serang, istirahat)').lower()
    if action_student == 'serang':
        hp_kuliah -= attack_student
        print(f'Student mengerjakan tugas, hp kuliah berkurang sebanyak {attack_student}')
    if action_student == 'istirahat':
        hp_student = 100
        print('Student istirahat, hp student pulih kembali')
    action_kuliah = random.randint(0,1)
    if action_kuliah == 0:
        hp_student -= attack_kuliah
        print(f'Deadline menghantui student, hp student berkurang sebanyak {attack_kuliah}')
    else:
        hp_kuliah += attack_kuliah
        print(f'Ada revisi, hp kuliah bertambah sebanyak {attack_kuliah}')
else:
    if hp_kuliah <= 0:
        print('Selamat Anda telah mengalahkan tugas-tugas kuliah')
    if hp_student <= 0:
        print('Sayang sekali Anda telah termakan oleh beratnya tugas kuliah')   
