import random

def registrasi(nama, kota, kategori):
    city = {'JAKARTA':'01', 'BEKASI':'02', 'LAINNYA':'03'}
    category = {'EMAS':'01', 'PERAK':'02', 'PERUNGGU':'03'}
    key = nama+':'+city[kota]+category[kategori]+str(random.randint(10000, 99999))
    return key

def cek_nama(nama, data):
    key = data.keys()
    for component in key:
        if component.find(nama) != -1:
            return component

def data_transfer(nas_kirim, nas_terima, nominal, data):
    try:
        nasabah_pengirim = cek_nama(nas_kirim.upper(), data)
        nasabah_penerima = cek_nama(nas_terima.upper(), data)
        if nasabah_pengirim != None:
            if nasabah_penerima != None:
                awal = nasabah_penerima.find(':')
                value = '%9s%07d' % (nasabah_penerima[awal+1:], nominal)
                data.setdefault(nasabah_pengirim, []).append(value)
                return True
            else:
                return False
        else:
            return False
    except AttributeError:
        return False
    except TypeError:
        return False

def save_transaction(data):    
    new_f = open('1806197342_transaction_log.txt', 'w')
    for key in data.keys():
        new_key = key.replace(':', ';')
        new_f.write(new_key+':\n')
        if data[key] == []:
            new_f.write('Belum pernah melakukan transaksi.\n')
        else:
            for value in data[key]:
                new_f.write(f'{value}\n')
    new_f.close()
