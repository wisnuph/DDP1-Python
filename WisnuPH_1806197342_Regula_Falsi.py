# Metode regular falsi/false positions
import modul_bisec
import math
import numpy as np

def fungsi(x, opsi):
    if opsi == 'a':
        return math.exp(0.2*x) - math.exp(-(0.8*x)) - 2
    elif opsi == 'b':
        return x*(math.sin(np.deg2rad(x))) - math.cos(np.deg2rad(x))
    elif opsi == 'c':
        return 1/((x-0.3)**2 + 0.01) - 1/((x-0.8)**2 + 0.04)
    else:
        return 1/(x*(math.exp(x))-20) + 8*math.exp(x)


def reg_falsi(f,x1,x2,tol=1.0e-6,maxfpos=10**6):
    xh = 0 ; fpos = 0
    if f(x1) * f(x2) > 0:
        return None
    else:
        for fpos in range(1, maxfpos+1):
            xh = x2 - (x2-x1)/(f(x2)-f(x1)) * f(x2)
            if abs(f(xh)) < tol:
                break
            elif f(x1) * f(xh) < 0:
                x2 = xh
            else:
                x1 = xh
        return xh, fpos

opsi = ''
while opsi != 'keluar':
    print('a. e^(0.2x)-e^(-0.8x)-2\nb. x.sin(x)-cos(x)\nc. 1/(x-0.3)^2+0.01 - 1/(x-0.8)^2+0.04\nd. 1/(x.e^x)-20 + 8^e^(x)')
    opsi = input('Masukkan opsi untuk fungsi yang akan dipilih (a/b/c/d/keluar): ')
    if opsi == 'a' or opsi == 'b' or opsi == 'c' or opsi == 'd':
        y = lambda x: fungsi(x,opsi)
        while True:
            try:
                x1 = float(input('Masukkan x1: '))
                x2 = float(input('Masukkan x2: '))
                if reg_falsi(y,x1,x2) == None:
                    print('Tidak ada akar diantara x1 dan x2\nCoba cari angka lain')
                else:
                    break
            except ValueError:
                print('Masukkan input dengan angka')
        i, j = reg_falsi(y,x1,x2)
        print('Nilai akar =',i, 'pada', j, 'false positions')
    else:
        if opsi == 'keluar':
            print()
        else:
            print('Masukkan input sesuai pilihan !')
print('Terima kasih telah menggunakan program ini')