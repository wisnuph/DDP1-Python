def get_data():
    try:
        data_file = open('input_order2.txt', 'r')
        order = []
        for line in data_file:
            order.append(line.split())
        return order
        data_file.close()
    except:
        return None

def list_order():
    data = get_data()
    if data == None:
        return None
    else:
        list_order = []
        for i in range(len(data)):
            list_order.append(data[i][1])
        list_order.sort(reverse=True)
        return list_order

def list_peringkat():
    order = list_order() ; data = get_data()
    if order == None and data == None:
        return None
    else:
        list_peringkat = []
        for i in range(len(order)):
            j = 0 
            while j < len(data):
                if data[j][1] == order[i]:
                    list_peringkat.append(data[j])
                    data.remove(data[j])
                j += 1
        return list_peringkat

peringkat = list_peringkat()
if peringkat == None:
    print('File tidak ditemukan')
else:
    try:
        masukkan = int(input())
        if 0 < masukkan < len(peringkat)+1:
            file_baru = open('TP7_output2_1806197342.txt', 'w')
            for i in range(masukkan):
                file_baru.write('{} {}\n'.format(peringkat[i][0], peringkat[i][1]))
            file_baru.close()
        else:
            print('Batasan input mulai dari 1 hingga 255')
    except:
        print('Input yang dimasukkan harus berupa bilangan bulat')