import urllib.request
import urllib.error

def get_data(alamat_url):
  # Implementasikan fungsi get_data di sini.
  # Jangan lupa menambahkan argumen yang sesuai.
  alamat_url_salah = True
  while alamat_url_salah:
    try:
      page = urllib.request.urlopen(alamat_url)
      text = page.read().decode('utf8')
      alamat_url_salah = False
      return text
    except:
      return None
  

def get_kereta(no_perjalanan):
  # Implementasikan fungsi get_kereta di sini.
  # Jangan lupa menambahkan argumen yang sesuai.
  text = get_data('https://skycruiser8.github.io/PF1Demo/kereta.html')
  index = text.find('<td>'+str(no_perjalanan))
  awal = text.find('<td>', index+1) 
  kereta = awal + len('<td>') ; akhir = text.find('</td>', kereta)
  if 1000<=no_perjalanan<=2500:
    if index != -1:
      return text[kereta:akhir]
    else:
      return None
  else:
    return None
  pass

def get_kepala_kereta(no_perjalanan):
  # Implementasikan fungsi get_kepala_kereta di sini.
  # Jangan lupa menambahkan argumen yang sesuai.
  text = get_data('https://skycruiser8.github.io/PF1Demo/sarana.html')
  if get_kereta(no_perjalanan) != None:
    index = text.find('<td>'+get_kereta(no_perjalanan)+'</td>')
    awal = text.find('<td>', index+1) ; akhir = text.find('</td>', awal)
    kepala_kereta = text[awal+len('<td>'):akhir]
    if index != -1:
      return(kepala_kereta)
    else:
      return None
  else:
    return None
  pass

def get_panjang_rangkaian(no_perjalanan):
  # Implementasikan fungsi get_panjang_rangkaian di sini.
  # Jangan lupa menambahkan argumen yang sesuai.
  text = get_data('https://skycruiser8.github.io/PF1Demo/sarana.html')
  if get_kereta(no_perjalanan) != None:
    index = text.find('<td>'+str(get_kereta(no_perjalanan))+'</td>')
    awal = text.find('<td>', index+1) ; akhir = text.find('</tr>', awal)
    if index != -1:
      return text.count('<td>', awal, akhir)
    else:
      return None
  else:
    return None
  pass
  
def main():
  # Jika kamu ingin menguji program secara manual, bisa di sini.
  # Fungsi ini tidak akan diuji oleh grader.
  pass

print(get_panjang_rangkaian(2241))