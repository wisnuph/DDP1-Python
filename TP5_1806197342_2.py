import urllib.request
import urllib.parse
def ambil_info_stok():
    page = urllib.request.urlopen('https://ceritanyatuwiter.herokuapp.com/1806197342')
    text = page.read().decode('utf8')
    where = text.find('Stok Terkini: ')
    awal = where + len('Stok Terkini: ') ; akhir = text.find('</p>')
    stok = int(text[awal:akhir])
    return(stok)

def laporkan_stok():
    stok_sekarang = str(stok - disalurkan)
    param = urllib.parse.urlencode({"pesan": "Stok Terkini: "+stok_sekarang})
    resp = urllib.request.urlopen("https://ceritanyatuwiter.herokuapp.com/1806197342/lapor", param.encode())

def request_stok():
    param = urllib.parse.urlencode({"pesan": "@burhan request restock untuk saya"})
    resp = urllib.request.urlopen("https://ceritanyatuwiter.herokuapp.com/1806197342/lapor", param.encode())

stok = ambil_info_stok()
disalurkan = 1

while disalurkan > 0:
    disalurkan = int(input('Berapa jumlah tabung yang ingin disalurkan: '))
    if disalurkan > 0:
        if (stok-disalurkan) == 0:
            print(f'Penyaluran {disalurkan} tabung berhasil, sekarang tersisa 0')
            request_stok()
        elif disalurkan > stok:
            print('Stok tidak cukup')
            request_stok()
        else:
            print(f'Penyaluran {disalurkan} tabung berhasil, sekarang tersisa {stok - disalurkan}')
            laporkan_stok()
    stok = ambil_info_stok()