import urllib.request
import json


### Panduan singkat
"""
Buat berkas lain yang memanggil library ini dengan perintah:
import kvdb

Pastikan saat registrasi string input parameter email 'myaccount@my.domain' diubah dengan email masing2. 
Simpan token nya baik-baik karena ini kunci untuk mengakses database selanjutnya.
Untuk memudahkan, fungsi db_registration akan sekalian membuat berkas yang berisi akses token anda. 

Usulan, koreksi dan pertanyaan terkait library ini bisa disampaikan ke ade@cs.ui.ac.id

Catatan: Library ini belum dilengkapi dengan exception handler (try except) yang baik. Peserta DDP1 akan menggunakan apa ada nya saat ini dan merasakan perlunya exception handler yang baik. Kita akan sama-sama memperbaikinya pada materi selanjutnya.
"""

### Contoh isi berkas yang menggunakan lain 
"""  main.py 
import kvdb

akses_key = kvdb.db_registration('myaccount@my.domain')  
kvdb.db_set(akses_key,'counter','0')
kvdb.db_set(akses_key,'name','Ini Nama Saya')
kvdb.db_set(akses_key,'email','myaccount@my.domain')
kvdb.db_get_key_list(akses_key)
kvdb.db_get_value(akses_key,'counter')
kvdb.db_get_value(akses_key,'email')
print(kvdb.db_get(akses_key))
"""


"""
Fungsi db_registrasi
Berguna untuk mendaftarkand diri agar dapat mengakses database kvdb
Input Parameter: 
    url_kvd:str adalah alamat URL dari layanan database kvdb.io
    registration:dict adalah pasangan key value dalam format 
    {'email':'myemail@some.domain'}
Outputnya adalah string yang menyatakan kunci khusus (token) untuk 
    dapat mengakses database selanjutnya.
Catatan: sebagai bagian dari side effect untuk membantu agar tidak lupa, 
         token yang diberikan, disimpan dalam berkas berakhir .kvdb_key
"""
def db_registration(email:str,
                    url_kvdb = 'https://kvdb.io'):
    encoded_registration = urllib.parse.urlencode({'email':email}).encode()
    page = urllib.request.urlopen(url_kvdb,data=encoded_registration)
    result = page.read().decode("utf8")
    file_key = open(email+".kvdb_key","w+")
    file_key.write("Token untuk akses ke database: " + url_kvdb)
    file_key.write("\nUntuk akun dengan email: " + email)
    file_key.write('\ntoken='+result)
    return result


"""
Fungsi db_get_key_list
Berguna untuk memperlihatkan data dalam bentuk pasangan key value (dictionary)
Input Parameter: 
    url_kvd:str adalah alamat URL dari layakan database key value
    token:str adalah kunci khusus (token) untuk dapat mengakses database,
          Kunci khusus (token) ini didapat setelah registrasi
Outputnya adalah list yang elemen nya adalah daftar key pada database
    contoh: ['hits','sarapan']
    yang menyatakan ada dua key yaitu 'hits' dan 'sarapan'
"""
def db_get_key_list(token:str,
                    url_kvdb = 'https://kvdb.io'):
    page = urllib.request.urlopen(url_kvdb+'/'+token+'/')
    result = page.read().decode("utf8")
    return result.split('\n')

"""
Fungsi db_get_value
Berguna untuk mengambil value sebuah key dari database
Input Parameter: 
    key:str adalah nama key yang ingin diambil valuenya
    url_kvd:str adalah alamat URL dari layakan database key value
    token:str adalah kunci khusus (token) untuk dapat mengakses database,
          Kunci khusus (token) ini didapat setelah registrasi
Outputnya adalah value dari key yang diminta dalam tipe string. 
    Kalaupun datanya bertipe bukan string, return nya tetap string. 
    Jadi lakukan casting atau konversi data sesuai kebutuhan.
    Bila key tersebut tidak tersedia akan mengeluarkan error exception.
    Pastikan check dulu bahwa key nya ada pada daftar key di database 
    sebelum mengambil value dari database.
"""
def db_get_value(token:str,
                 key:str,
                 url_kvdb = 'https://kvdb.io'):
    page = urllib.request.urlopen(url_kvdb+'/'+token+'/'+key)
    result = page.read().decode("utf8")
    return result

"""
Fungsi db_set
Berguna untuk mengisi data pasangan key dan value. 
Bila sudah ada value-nya di update. Bila belum ada dibuat baru.
Input Parameter: 
    key:str adalah nama key 
    value:str adalah value yang ingin disimpan. 
              Kalau pun datanya integer tetap harus di-casting dulu ke string
    url_kvd:str adalah alamat URL dari layakan database key value
    token:str adalah kunci khusus (token) untuk dapat mengakses database,
          Kunci khusus (token) ini didapat setelah registrasi
Outputnya tidak ada. Fungsi ini melakukan perubahan pada database sebagai side effect, 
    tidak memberikan output ke program secara langsung. 
"""
def db_set(token:str,
           key:str,
           value:str,
           url_kvdb = 'https://kvdb.io'):
    encoded_value = value.encode()
    page = urllib.request.urlopen(url_kvdb+'/'+token+'/'+key,data=encoded_value)


"""
Fungsi db_get
Berguna untuk memperlihatkan data dalam bentuk pasangan key value (dictionary)
Input Parameter: 
    url_kvd:str adalah alamat URL dari layakan database key value
    token:str adalah kunci khusus (token) untuk dapat mengakses database,
          Kunci khusus (token) ini didapat setelah registrasi
Outputnya adalah seluruh isi database dalam tipe dictionary. 
    Bila ada value yang isinya integer maka akan otomatis dicasting jadi integer. 
"""
def db_get(token:str,
           url_kvdb = 'https://kvdb.io'):
    page = urllib.request.urlopen(url_kvdb+'/'+token+'/?values=true&format=json')
    result = json.loads(page.read().decode("utf8"))
    return dict(result)


### Akhir berkas. 
### Saran, koreksi dan pertanyaan hubungi: Ade Azurat (ade@cs.ui.ac.id)