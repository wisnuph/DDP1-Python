import urllib.request

url_1 = 'https://pudyprima.github.io/covid19-jakarta'
url_2 = 'https://pudyprima.github.io/covid19-indonesia'

def get_data(alamat_url):
    try:
        page = urllib.request.urlopen(alamat_url)
        text = page.read().decode('utf8')
        return text
    except:
        return 'Alamat url salah/koneksi internet mati'

def get_data_per_wilayah(nama_wilayah):
    text = get_data(url_1).lower()
    nama_wilayah = nama_wilayah.lower()
    index = text.find('<td>'+nama_wilayah)
    where = text.find('<td>', index+1)
    awal = where + len('<td>') ; akhir = text.find('</td>', awal)
    if index != -1:
        return text[awal:akhir]
    elif text == 'Alamat url salah/koneksi internet mati':
        return 'Url salah/koneksi tidak ada'
    else:
        return 'Data tidak ditemukan!\nInput nama wilayah tidak sesuai'
    
def get_data_jakarta():
    try:
        list_wilayah = ['Utara', 'Timur', 'Selatan', 'Barat', 'Pusat']
        total = 0
        for wilayah in list_wilayah:
            data = int(get_data_per_wilayah('Jakarta '+wilayah))
            total += data
        return total
    except:
        return 'Data tidak ditemukan atau alamat url tidak cocok'

def get_total_kasus_by_keyword(kategori):
    text = get_data(url_2)
    return text.count(kategori)

print(get_total_kasus_by_keyword('perempuan'))
