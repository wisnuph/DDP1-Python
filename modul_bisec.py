def bisection(a,b,tol,y):
    beda = abs(a-b)
    while beda > tol:
        c = (a+b)/2
        if y(a)*y(c) > 0:
            a = c
        else:
            b = c
        beda = abs(a-b)
    akar = (a+b)/2
    return akar