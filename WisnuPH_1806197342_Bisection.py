# Metode Bisections
import modul_bisec
import math
import numpy as np

def fungsi(x, opsi):
    if opsi == 'a':
        return math.exp(0.2*x) - math.exp(-(0.8*x)) - 2
    elif opsi == 'b':
        return x*(math.sin(np.deg2rad(x))) - math.cos(np.deg2rad(x))
    elif opsi == 'c':
        return 1/((x-0.3)**2 + 0.01) - 1/((x-0.8)**2 + 0.04)
    else:
        return 1/(x*(math.exp(x))-20) + 8*math.exp(x)

opsi = ''
while opsi != 'keluar':
    print('a. e^(0.2x)-e^(-0.8x)-2\nb. x.sin(x)-cos(x)\nc. 1/(x-0.3)^2+0.01 - 1/(x-0.8)^2+0.04\nd. 1/(x.e^x)-20 + 8^e^(x)')
    opsi = input('Masukkan opsi untuk fungsi yang akan dipilih (a/b/c/d/keluar): ')
    if opsi == 'a' or opsi == 'b' or opsi == 'c' or opsi == 'd':
        y = lambda x: fungsi(x, opsi)
        while True:
            try:
                a = float(input('Masukkan nilai a: '))
                b = float(input('Masukkan nilai b: '))
                if y(a)*y(b) > 0:
                    print('Tidak ada akar diantara a dan b\nCoba angka yg lain')
                else:
                    break
            except ValueError:
                print('Masukkan input dengan angka')    
        tol = float(input('Masukkan toleransi: '))
        print(modul_bisec.bisection(a,b,tol,y))
    else:
        if opsi == 'keluar':
            print()
        else:
            print('Masukkan input sesuai pilihan !')
print('Terima kasih telah menggunakan program ini')